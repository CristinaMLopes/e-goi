var ar, ApiRequest = {
    apiUrl: 'http://localhost:8002',
    getCategoriesList: function(name = null) {
        let urlName = name ? '?name=' + name : '';
        $.ajax({
            url: this.apiUrl + '/categories' + urlName,
            type: 'GET',
            crossDomain: true,
            success: function (data) {
                Categories.getCategoriesListAction(data);
            },
            error: function (err) {
                toastr.error('Ocorreu um problema no seu pedido. Por favor volte a tentar.', 'Problema no pedido!');
            }
        });
    },
    editCategorySubmit: function (info, id) {
        $.ajax({
            url: this.apiUrl + '/category/' + id,
            type: 'PUT',
            data: info,
            crossDomain: true,
            success: function (data) {
                Categories.showList();
                toastr.success('A sua categoria foi editada com sucesso.', 'Sucesso!');
            },
            error: function (err) {
                toastr.error('Ocorreu um problema no seu pedido. Por favor volte a tentar.', 'Problema no pedido!');
            }
        });
    },
    createCategorySubmit: function (info) {
        $.ajax({
            url: this.apiUrl + '/category',
            type: 'POST',
            data: info,
            crossDomain: true,
            success: function (data) {
                Categories.showList();
                toastr.success('A sua categoria foi criada com sucesso.', 'Sucesso!');
            },
            error: function (err) {
                toastr.error('Ocorreu um problema no seu pedido. Por favor volte a tentar.', 'Problema no pedido!');
            }
        });
    },
    editCategory: function (id) {
        $.ajax({
            url: this.apiUrl + '/category/' + id,
            type: 'GET',
            dataType: "json",
            crossDomain: true,
            success: function (data) {
                Categories.editCategoryAction(data);
            },
            error: function (err) {
                toastr.error('Ocorreu um problema no seu pedido. Por favor volte a tentar.', 'Problema no pedido!');
            }
        });
    },
    deleteCategory: function (id) {
        $.ajax({
            url: this.apiUrl + '/category/' + id,
            type: 'DELETE',
            crossDomain: true,
            success: function (data) {
                toastr.success('A sua categoria foi apagada com sucesso.', 'Sucesso!');
                Categories.refreshList();
            },
            error: function (err) {
                toastr.error('Ocorreu um problema no seu pedido. Por favor volte a tentar.', 'Problema no pedido!');
            }
        });
    },

}