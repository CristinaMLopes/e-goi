var cat, Categories = {
    variables: {
        inputName: ".input-field[name='username']",
        select2Subcategories: ".subcategories-select2",
        listPage: ".list-page",
        createCategory: "#create-category",
        searchCategory: "#search-category",
        searchCategoryInput: "#search-input",
        deleteFilter: ".delete-filter",
        noCategories: ".no-categories",
        categoryBox: ".container-category-box",
        categoryBoxPrototype: ".container-category-box-prototype",
        deleteCategory: ".delete-category",
        editCategory: ".edit-category",
        createCategoryPage: ".create-category",
        createCategoryForm: ".save-category",
        saveCategoryButton: "#save-category",
        backButton: "#back"
    },
    initPageBehaviour: function () {
        cat = Categories.variables;
        ApiRequest.getCategoriesList();
        this.initListPage();
        this.initCreatePage();

        toastr.options = {
            "positionClass": "toast-bottom-right",
        }
    },
    hideShow: function (hide, show) {
        $(hide).hide();
        $(show).show();
    },
    generateCategoryList: function (data) {
        let category, subcategories;
        let text = "";
        let subcategoriesText = "<ul>";
        for (i = 0; i < data.length; i++) {

            category = $(".row-categories").clone();

            category.find(cat.categoryBoxPrototype).addClass("container-category-box").removeClass("container-category-box-prototype").removeClass('hide');
            category.find(".category-title").text("#" + data[i]["categoryId"]);
            category.find(".info-name").text(data[i]["name"]);
            category.find(".info-created").text(moment(data[i]["created"]).format("DD-MM-YYYY HH:mm"));
            category.find(".info-modified").text(moment(data[i]["modified"]).format("DD-MM-YYYY HH:mm"));
            category.find(cat.editCategory).attr("_id", data[i]["categoryId"]);
            category.find(cat.deleteCategory).attr("_id", data[i]["categoryId"]);

            if (data[i]['subcategories']) {
                subcategories = (data[i]['subcategories']).split(',');
                for (let j = 0; j < subcategories.length; j++) {
                    subcategoriesText += `<li class="subcategories-li">${subcategories[j]}</li>`;
                }
                subcategoriesText += "</ul>"
            }
            else {
                subcategoriesText = "<span class=\"no-subcategories\">Não existem subcategorias associadas.</span>"
            }

            category.find(".info-subcategories").html(subcategoriesText);
            text += category.html();
            subcategoriesText = "<ul>";
        }

        $(cat.categoryBoxPrototype).after(text);
        this.actionsCategory();
    },
    actionsCategory: function () {
        // DELETE ACTIONS (confirm, delete, refresh list)
        $(cat.deleteCategory).on("click", function () {

            let id = $(this).attr('_id');
            Swal.fire({
                title: 'Tem a certeza?',
                text: "Irá apagar a categoria e as suas subcategorias",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#364756',
                cancelButtonColor: '#aaaaaa',
                confirmButtonText: 'Sim, apagar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    ApiRequest.deleteCategory(id);
                }
            })
        });

        // EDIT ACTIONS (Change to edit page)
        $(cat.editCategory).on("click", function () {
            Categories.clearCreatePage();

            let id = $(this).attr('_id');
            $(cat.createCategoryForm).attr('_id', id);
            ApiRequest.editCategory(id);
            Categories.hideShow(cat.listPage, cat.createCategoryPage);
        });

    },
    clearCreatePage: function () {
        $(cat.inputName).val("");
        $(cat.select2Subcategories + " option").remove();
        $(cat.select2Subcategories).val(null).trigger('change');
        $(cat.createCategoryForm).attr('_id', "");
    },
    initCreatePage: function () {
        // Subcategories select2 init
        $(cat.select2Subcategories).select2({
            placeholder: "Escreva os nomes das subcategorias",
            tags: true,
            multiple: true
        });

        $(cat.backButton).on("click", function () {
            Categories.hideShow(cat.createCategoryPage, cat.listPage);
            Categories.refreshList();
        });

        $(cat.saveCategoryButton).on("click", function () {
            if (!Categories.validateForm()) 
                return false;

            let info = { name: $(cat.inputName).val(), subcategories: $(cat.select2Subcategories).select2("val") };
            let id = $(cat.createCategoryForm).attr('_id');
            (id) ? ApiRequest.editCategorySubmit(info, id) : ApiRequest.createCategorySubmit(info);
        });
    },
    validateForm: function () {
        const name = $(cat.inputName).val();

        if (name == "") {
            toastr.error('Por favor preencha todos os campos para proceder.', 'Preencha todos os campos!');
            return false;
        }

        return true;
    },
    refreshList: function (name = null, clean = true) {
        $(cat.categoryBox).remove();
        if(clean) $(cat.searchCategoryInput).val("");
        $(cat.noCategories).addClass('hide');
        ApiRequest.getCategoriesList(name);
    },
    showList: function () {
        this.hideShow(cat.createCategoryPage, cat.listPage);
        this.refreshList();
    },
    initListPage: function () {
        $(cat.createCategory).on("click", function () {
            Categories.hideShow(cat.listPage, cat.createCategoryPage);
            Categories.clearCreatePage();
        });

        $(cat.searchCategory).on("click", function () {
            if($(cat.searchCategoryInput).val() != "")
                Categories.refreshList($(cat.searchCategoryInput).val(),false);
        });

        $(cat.deleteFilter).on("click", function () {
            Categories.refreshList();
            $(cat.searchCategoryInput).val("");
        });
    },
    getCategoriesListAction: function (data) {
        let categories = JSON.parse(data)['categories'];
        if (categories.length > 0)
            Categories.generateCategoryList(categories);
        else
            $(cat.noCategories).removeClass('hide');
    },
    editCategoryAction: function (data) {
        $(cat.inputName).val(data.category.name);

        if (data.category.subcategories) {
            subcategoriesList = data.category.subcategories.split(',');

            for (let i = 0; i < subcategoriesList.length; i++)
                $(cat.select2Subcategories).append(`<option value="${subcategoriesList[i]}">${subcategoriesList[i]}</option>`)

            $(cat.select2Subcategories).val(subcategoriesList);
            $(cat.select2Subcategories).trigger('change');
        }
    }
};

$(window).on('load', function () {
    Categories.initPageBehaviour(); // Init page
});