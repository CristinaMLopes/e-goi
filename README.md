# README #

Este documento README ajuda a entender o que foi feito nesta aplicação de gestão de categorias e subcategorias.

### Para que serve este repositório? ###

* API com base de dados & Front
Criar uma API para gestão de categorias e subcategorias.
A API deve implementar 5 endpoints para que seja possível executar todas as funções básicas (CRUD).
No endpoint para listagem de todos os registros deverá ser possível fazer uma busca pelo nome da categoria.

### Como fazer o setup? ###

* Fazer download ou clone deste repositório
* Abrir a consola e ir para a pasta da API do projeto
* Correr o comando docker-compose-up -d para arrancar a BD (é necessário ter o Docker e o PHP instalado)
* Aceder à BD via SequelPro (por exemplo) - localhost:3310 root/root
* Correr o [script](https://bitbucket.org/CristinaMLopes/e-goi/src/master/E-Goi%20API/db_structure.sql) com as tabelas da BD
* Arrancar a API/servidor com o comando php -s localhost:8002
* Abrir o ficheiro base.html da pasta do FE do projeto

### O que assumi/fiz ###

* Uma subcategoria não é uma categoria
* Tanto a categoria como a subcategoria têm um nome, uma data de criação e uma de modificação. A subcategoria ainda tem um categoryId que representa o parent dela.
* A aplicação do FE foi toda feita numa SPA (Single Page Application). Tem o ficheiro html, o css (com a parte de design) e dois javascripts (o main.js e o api_request.js).
* 5 endpoints: listagem/filtro de categorias (GET /categories ou /categories?name=categoria), criação de uma categoria (POST /category), edição de uma categoria (PUT /category/:id), apagar uma categoria (DELETE /category/:id), ir buscar informação sobre uma categoria específica (GET /category/:id).
* A API foi feita em PHP com a framework Slim. Para a comunicação com a BD utilizei um DBO para ir buscar e enviar informação.
* O FE foi todo feito em HTML, Javascript (jQuery com pedidos AJAX ao servidor) e CSS. Utilizei alguns plugins como o MomentJS (para datas), o select2 (para dropdowns), o Toastr (para mensagens de sucesso e erro), o SweetAlert2 (para popups), o FontAwesome (para icones) e o Bootstrap (para ficar responsive).

### Estrutura da BD ###

* 2 Tabelas: categories e sub_categories
![Estrutura BD](https://world-toexplore.com/wp-content/uploads/2020/06/Estrutura-DB.png)

### Demonstração ###

![Demonstração](https://i.ibb.co/gyYKhk1/ezgif-com-video-to-gif.gif)


