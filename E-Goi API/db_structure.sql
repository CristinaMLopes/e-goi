DROP DATABASE IF EXISTS EGOI;
CREATE DATABASE EGOI;
use EGOI;

CREATE TABLE EGOI.categories (
    categoryId int(11) unsigned NOT NULL auto_increment,
    name varchar(100) NOT NULL,
    created datetime NOT NULL DEFAULT current_timestamp(),
    modified datetime NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (categoryId)
);

CREATE TABLE EGOI.sub_categories (
    subCategoryId int(11) unsigned NOT NULL auto_increment,
    name varchar(100) NOT NULL,
    created datetime NOT NULL DEFAULT current_timestamp(),
    modified datetime NOT NULL DEFAULT current_timestamp(),
    categoryId int(11) unsigned NOT NULL,
    PRIMARY KEY (subCategoryId),
    KEY `fk_categoryId_idx` (`categoryId`),
    CONSTRAINT `fk_categoryId` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`) ON DELETE CASCADE
);