<?php

require_once './service.php';

class Controller
{
    public $service; 

    function __construct()
	{
		$this->service = new Service();
    }

    /**
     * Get all the categories (or get category by name)
     */
	function getCategories($request, $response)
	{
        $nameParam = $request->getQueryParams()["name"];

        $result = $this->service->getCategories($nameParam);

        $res = ['categories' => $result];
        $response->getBody()->write(json_encode($res));
    
        return $response;
    }
    
    /**
     * Get a category by the ID
     */
    function getCategoryById($request, $response, $args)
	{
        $id = $args["id"];

        $result = $this->service->getCategoryById($id);

        $res = ['category' => $result];
        $response->getBody()->write(json_encode($res));

        return $response;
    }
    
    /**
     * Create a new category and the subcategories
     */
    function createCategory($request, $response, $args)
	{
        $body = $request->getParsedBody();

        $result = $this->service->createCategory($body);

        $res = ['id' => $result];
        $response->getBody()->write(json_encode($res));
    
        return $response;
    }
    
    /**
     * Edit a category (delete and insert again the subcategories)
     */
    function editCategory($request, $response, $args)
	{
        $body = $request->getParsedBody();
        $id = $args["id"];

        $result = $this->service->editCategory($body, $id);

        $res = ['id' => $result];
        $response->getBody()->write(json_encode($res));

        return $response;
    }
    
    /**
     * Delete a category by id
     */
    function deleteCategory($request, $response, $args)
	{
        $id = $args["id"];

        $this->service->deleteCategory($id);

        $res = ['id' => $id];
        $response->getBody()->write(json_encode($res));
    
        return $response;	}

}