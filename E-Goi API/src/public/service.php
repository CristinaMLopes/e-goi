<?php

class Service
{
    public $conn;
    public $db;

    function __construct()
	{
		$this->conn = new DBConnection();
        $this->db = $this->conn->mConnect();
    }
    
	function getCategories($nameParam)
	{
        $query = "SELECT c.*, GROUP_CONCAT(sc.name) as subcategories 
                FROM categories c 
                LEFT JOIN sub_categories sc ON c.categoryId = sc.categoryId";

        if(isset($nameParam))
            $query .= " WHERE c.name LIKE \"%".$nameParam."%\"";
                
        $query .= " GROUP BY c.categoryId";

        $res = $this->db->query($query);

        $result = $res->fetch_all(MYSQLI_ASSOC);

        return $result;
    }
    
    function getCategoryById($id)
	{
        $query = "SELECT c.*, GROUP_CONCAT(sc.name) as subcategories 
                FROM categories c 
                LEFT JOIN sub_categories sc ON c.categoryId = sc.categoryId 
                WHERE c.categoryId = " . $id . " GROUP BY c.categoryId";
        
        $res = $this->db->query($query);

        $result = $res->fetch_assoc();

        return $result;
    }
    
    function createCategory($body)
	{
        $createCategoryQuery = "INSERT INTO categories(name) VALUES (\"" . $body["name"] . "\")";

        if ($this->db->query($createCategoryQuery)) {
            $id = $this->db->insert_id;
            $this->createSubcategories($id, $body["subcategories"]);
        }

        return $id;
    }

    function editCategory($body, $id)
	{
        $updateCategoryQuery = "UPDATE categories SET modified = CURRENT_TIMESTAMP(), name=\"" . $body["name"] . "\" WHERE categoryId = " . $id;

        // TODO:: transaction
    
        $deleteSubCategoriesQuery = "DELETE FROM sub_categories WHERE categoryId = " . $id;
    
        $this->db->query($updateCategoryQuery);
        $this->db->query($deleteSubCategoriesQuery);
        $this->createSubcategories($id, $body["subcategories"]);
    }

    function deleteCategory($id) {

        $deleteCategoriesQuery = "DELETE FROM categories WHERE categoryId = " . $id;
        $this->db->query($deleteCategoriesQuery);
    }
    
    function createSubcategories($categoryId, $subcategories)
	{
        if (count((array) $subcategories) > 0) {
            $createSubcategoriesQuery = "INSERT INTO sub_categories(name,categoryId) VALUES ";
    
            for ($i = 0; $i < count((array) $subcategories); $i++) {
                if ($i != 0) {
                    $createSubcategoriesQuery .= ", ";
                }
                $createSubcategoriesQuery .= "(\"" . $subcategories[$i] . "\"," . $categoryId . ")";
            }
            $this->db->query($createSubcategoriesQuery);
        }
    }

}