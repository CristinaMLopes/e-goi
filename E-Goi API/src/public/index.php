<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';
require_once '../include/DBOps/DBConnection.php';
require_once './controller.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Auth-Token');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');

$conn = new DBConnection();
$db = $conn->mConnect();

$controller = new Controller();

$app = AppFactory::create();
$app->addBodyParsingMiddleware(); 
$app->addErrorMiddleware(true, true, true);

$app->get('/categories', function (Request $request, Response $response, $args) use ($db, $controller) {

    return $controller->getCategories($request, $response);
});

$app->get('/category/{id}', function (Request $request, Response $response, $args) use ($db, $controller) {

    return $controller->getCategoryById($request, $response, $args);
});

$app->post('/category', function (Request $request, Response $response, $args) use ($db, $controller) {

    return $controller->createCategory($request, $response, $args);
});

$app->put('/category/{id}', function (Request $request, Response $response, $args) use ($db, $controller) {

    return $controller->editCategory($request, $response, $args);
});

$app->delete('/category/{id}',function(Request $request, Response $response, array $args) use ($db, $controller) {

    return $controller->deleteCategory($request, $response, $args);
});

$app->run();